const {resolve} = require('path')
const config = require('./src/config/index-vite.js')
var obj = {
	alias: {
         '/@/': resolve(__dirname, './src')
    },
	proxy:{
		
	}
}
obj.proxy[config.baseUrl.dev] = {
	target: config.localUrl,
    changeOrigin: true,
    rewrite: path => path.replace(/^\/api/, '')
}
export default obj