/**
 * Created by zhoushg on 2017/9/25.
 */
/**
 * Created by zhoushg on 2017/9/8.
 */
var shortid = require('shortid')
var tableConfig = {

  dfTable: {
    _id: {
      type: String,
      unique: true,
      default: shortid.generate
    }
  }
  // addCode
			// book Start
			,book:{
        _id: {
          type: String,
          unique: true,
          default: shortid.generate
        },
        userId: String,
        createTime: {
          type: Date,
          default: new Date()
        },
        updateTime: {
          type: Date,
          default: new Date()
        },
          name:String, // 书名
          price:Number, // 价格
          type:String, // 类型
          isCollection:Boolean, // 是否收藏
          author:String // 作者
          
        }
			// book end
			
			// test3 Start
			,test3:{
        _id: {
          type: String,
          unique: true,
          default: shortid.generate
        },
        userId: String,
        createTime: {
          type: Date,
          default: new Date()
        },
        updateTime: {
          type: Date,
          default: new Date()
        },
          name:String, // 添加测试
          age:String // 年龄
          
        }
			// test3 end
			
			// test Start
			,test:{
        _id: {
          type: String,
          unique: true,
          default: shortid.generate
        },
        userId: String,
        createTime: {
          type: Date,
          default: new Date()
        },
        updateTime: {
          type: Date,
          default: new Date()
        },
          name:{ // 姓名
            type:String,
            default:'',
            unique: true
            },
          type:String, // 类型
          age:Number, // 年纪
          arr:Array, // 数组类型测试
          obj:Object, // 对象类型测试
          bol:Boolean, // bool类型测试
          date:Date // 时间类型测试
          
        }
			// test end
			
			// userInfo Start
		,userInfo:{
			_id: {
			  type: String,
			  unique: true,
			  default: shortid.generate
			},
			createTime: {
			  type: Date,
			  default: new Date()
			},
			updateTime: {
			  type: Date,
			  default: new Date()
			},
			userId:{ // 用户id
				type:String,
				default:'',
				unique: true
			},
			  name:String, // 用户名称
			  access:{
				  type: Array,
				  default: ['admin']
			  }, // 用户权限
			  roles:{
				  type: Array,
				  default: ['admin']
			  },
			  token:String, // token
			  introduction:String,
			  avatar:String, // 用户图标
        }
		// userInfo end
  // user Start
		,user:{
			_id: {
			  type: String,
			  unique: true,
			  default: shortid.generate
			},
			userId: String,
			createTime: {
			  type: Date,
			  default: new Date()
			},
			updateTime: {
			  type: Date,
			  default: new Date()
        },
          username:{ // 用户名
            type:String,
            default:'',
            unique: true
            },
          phone:String, // 电话
          password:String, // 密码
          code:{ // 验证码等等
            type:String,
            default:''
            },
          token:{ // token
            type:String,
            default:''
            },
          date:{ // 最后一次上线时间
            type:String,
            default:new Date
            },
          email:{ // 邮箱
            type:String,
            default:''
            }
          
        }
		// user end

}
exports.tableConfig = tableConfig;
