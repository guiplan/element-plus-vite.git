/**
 * Created by zhoushg on 2017/9/8.
 */
 var mongodb = {
  db_user: "admin", // 自己服务器数据库账号
  db_pwd: "admin", // 自己服务器数据库密码
  db_host: "127.0.0.1",
  db_port: 27017,
  db_name: "test-component"
}
 if(process.env.server == "local"){
	mongodb = {
	  db_user: "",
	  db_pwd: "",
	  db_host: "127.0.0.1",
	  db_port: 27017,
	  db_name: "test-component"
	}
 }
exports.mongodb = mongodb
exports.Config = {
  outTime:1200 // 登录超时时间
}

exports.isCrossDomain = true // 是否支持跨域
