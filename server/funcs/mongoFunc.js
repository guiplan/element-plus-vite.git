var send = require('./sendData')
var mongodb = require('../plugs/mongose/mongoQuery')
var Config = require('../config/index').Config
var result = {
  getId(table, where, res) { // 获取id
    return new Promise(bk => {
      mongodb.findOne(table, where, function (err, data) {
        if (err) {
          bk(false)
          return
        }
        if (data) {
          if (data._id) {
            bk(data._id)
          } else {
            bk(false)
          }
        } else {
          bk(false)
        }
      })
    })
  }
}
module.exports = result
