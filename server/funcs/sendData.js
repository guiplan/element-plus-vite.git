/**
 * Created by zhoushg on 2017/9/7.
 */
/**
 * 发送数据
 * @param {Object} state 状态
 * @param {Object} data 内容
 */
var send = function (res, _state, data) { // 直接弹出的不用加id
 var state =  20000;
  if (_state == true&& data) {
    res.send({
      "returnFlg": true,
	  "status":state,
	  "code":state,
      "data": data
    });
  } else {
    if (!data) data = "查询错误！请重试";
    var sendData = "查询错误！请重试";
    switch (data) {//根据错误码 返回不同的数据
      case 1:
        sendData = {
          errId: data,
          msgStr: "登录超时，请重新登录"
        }
        break;
      case 2:
        sendData = {
          errId: data,
          msgStr: "没查到数据"
        }
        break;
      case 3:
        sendData = {
          errId: data,
          msgStr: "当前项目名已存在，请重新命名"
        }
        break;

      default:
        sendData = data
        break;
    }
	state = 40000
    res.send({
      "returnFlg": false,
	  "status":state,
	  "code":state,
      "errMsg": sendData,
	  "data":''
    });
  }
}
module.exports = send
