// getInterface
var server = require('./interface/server.js')
var book = require('./interface/book.js')
var test3 = require('./interface/test3.js')
var registerDemo = require('./interface/registerDemo.js')
var message = require('./interface/message.js')
var userInfo = require('./interface/userInfo.js')
var notice = require('./interface/notice.js')
var user = require('./interface/user.js')
var cookieParser = require('cookie-parser')
var isCrossDomain = require('./config/index').isCrossDomain // 是否支持跨域
var setInterFace = app => {
  // 设置跨域访问
  if (isCrossDomain) {
    app.all('*', function (req, res, next) {
      res.header('Access-Control-Allow-Origin', '*')
      res.header('Access-Control-Allow-Headers', 'X-Requested-With')
      res.header('Access-Control-Allow-Methods', 'PUT,POST,GET,DELETE,OPTIONS')
      res.header('X-Powered-By', ' 3.2.1')
      res.header('Content-Type', 'application/json;charset=utf-8')
	  res.header('Access-Control-Allow-Credentials','true')
	
      next()
    })
  } else {
    app.use(cookieParser()) // 使用cookie
  }
  var bodyParser = require('body-parser')
  // app.use(express.static(path.join(__dirname, 'static'))) // 使用静态文件位置
  app.use(bodyParser.json({
    limit: '10mb'
  }))
// 这里指定参数使用 json 格式
  app.use(bodyParser.urlencoded({
    extended: true,
    limit: '50mb'
  }))
// useInterface
server(app)
book(app)
test3(app)
registerDemo(app)
message(app)
userInfo(app)
  notice(app)
  user(app)
}
module.exports = setInterFace
