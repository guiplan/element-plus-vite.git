var required = require('../plugs/guimain/required')
var userTimeRun = require('../funcs/loginTimeOut')
var mongoFunc = require('../funcs/mongoFunc')
var send = require('../funcs/sendData')
var mongodb = require('../plugs/mongose/mongoQuery')
var result = app => {
	var isEmptyStr = required.isEmptyStr
	var isRange = required.isRange
	var isEmptyObject = required.isEmptyObject
	// addCode
			// r1jhZ0H7V Start

app.get('/user/info',(req, res) => { // userInfo-查询

  var params = req.query

  var where = {
      }



  var token =  params.token
  userTimeRun(token, function (userData) {
    if (userData) {
        where.userId = userData._id

  mongodb.findOne('userInfo', where,function(err, data) {
    if(err) {
      console.log(err)
      send(res, false)
    } else {
      send(res, true, data)
    }
    return
  })

         }
    }, res)
})
			// r1jhZ0H7V end

			// rJFpaEmfE Start

app.post('/userInfo/add',(req, res) => { // userInfo-新增

  var params = req.body
  var name = params.name // 用户名称
  var access = params.access // 权限
    if(!access){
      access = ['super_admin', 'admin']
    }
  var avator = params.avator // 用户图标

  var where = {
      name,
      access,
      avator}


    where.createTime = new Date()
    where.updateTime = new Date()

  var token =  params.token
  userTimeRun(token, function (userData) {
    if (userData) {
        where.userId = userData._id
    	var userId = where.userId
     var where2 = { $or:[{userId}] }
         mongodb.findOne('userInfo', where2,function(err, data) {
            if(err) {
              console.log(err)
              send(res, false)
              return
            }
            if(data){
                send(res, false,"输入重复")
                return
             }else{
               where.token = userData.token

  mongodb.save('userInfo', where,function(err, data) {
    if(err) {
      console.log(err)
      send(res, false)
    } else {

      send(res, true, data)
    }
    return
  })
    }})

         }
    }, res)
})
		// rJFpaEmfE end

			// BJYMhEQM4 Start

app.get('/get_info',(req, res) => { // userInfo-查询

  var params = req.query

  var where = {
      }



  var token =  params.token
  userTimeRun(token, function (userData) {
    if (userData) {


  mongodb.find('userInfo', where,function(err, data) {
    if(err) {
      console.log(err)
      send(res, false)
    } else {

      send(res, true, data)
    }
    return
  })


         }
    }, res)
})
		// BJYMhEQM4 end

}
module.exports = result
