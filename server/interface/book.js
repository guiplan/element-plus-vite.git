var required = require('../plugs/guimain/required')
var authToken = require('../funcs/authTimeOut')
var mongoFunc = require('../funcs/mongoFunc')
var send = require('../funcs/sendData')
var mongodb = require('../plugs/mongose/mongoQuery')
var result = app => {
	var isEmptyStr = required.isEmptyStr
	var isRange = required.isRange
	var isEmptyObject = required.isEmptyObject
	// addCode
			// XcaY-YJxQ Start
			

app.post('/book/update',authToken,(req, res) => { // book-修改
    
  var params = req.body
  var _id = params._id // 列表唯一id
    if (isEmptyStr(_id)) {
      send(res, false, '列表唯一id不能为空')
      return
    }
  var name = params.name // 书名
  var price = params.price // 价格
  var type = params.type // 类型
  var isCollection = params.isCollection // 是否收藏
  var author = params.author // 作者
    
  var where = {_id}
    
  var updateParams = {name,price,type,isCollection,author}
    
    updateParams.updateTime = new Date()
    
        where.userId = req.userId
    mongoFunc.getId('book', where, res).then(_data=>{
      if(_data){
      
    
  mongodb.update('book', where,updateParams,function(err, data) {
    if(err) {
      console.log(err)
      send(res, false)
    } else {
      
      send(res, true, data)
    }
    return
  })
    }else{
        send(res, false,'id没找到')
      }
    })
    
})
			// XcaY-YJxQ end
			
			// Xw2-lJNxS Start
			

app.post('/book/delete',authToken,(req, res) => { // book-删除
    
  var params = req.body
  var _id = params._id // 数据唯一id
    if (isEmptyStr(_id)) {
      send(res, false, '数据唯一id不能为空')
      return
    }
    
  var where = {_id}
    
    
    
        where.userId = req.userId
    mongoFunc.getId('book', where, res).then(_data=>{
      if(_data){
      
    
      if(isEmptyObject(where)){
        send(res, false);
        return
      } 
  mongodb.remove('book', where,function(err, data) {
    if(err) {
      console.log(err)
      send(res, false)
    } else {
      
      send(res, true, data)
    }
    return
  })
    }else{
        send(res, false,'删除失败，不可重复删除')
      }
    })
    
})
			// Xw2-lJNxS end
			
			// T1jQwK0u9 Start
			

app.post('/book/add',authToken,(req, res) => { // book-新增
    
  var params = req.body
  var name = params.name // 书名
  var price = params.price // 价格
  var type = params.type // 类型
  var isCollection = params.isCollection // 是否收藏
  var author = params.author // 作者
    
  var where = {name,price,type,isCollection,author}
    
    
    where.createTime = new Date()
    where.updateTime = new Date()
    
        where.userId = req.userId
    
    
  mongodb.save('book', where,function(err, data) {
    if(err) {
      console.log(err)
      send(res, false)
    } else {
      
      send(res, true, data)
    }
    return
  })
    
    
})
			// T1jQwK0u9 end
			
			// EyuiyfMJs Start
			

app.get('/book/data',authToken,(req, res) => { // book-查询
    
  var params = req.query
    
  var where = {}
    
    
    
    
    
  mongodb.findOne('book', where,function(err, data) {
    if(err) {
      console.log(err)
      send(res, false)
    } else {
      
      send(res, true, data)
    }
    return
  })
    
    
})
			// EyuiyfMJs end
			
}
module.exports = result
