﻿var required = require('../plugs/guimain/required')
var authToken = require('../funcs/authTimeOut')
var mongoFunc = require('../funcs/mongoFunc')
var send = require('../funcs/sendData')
var mongodb = require('../plugs/mongose/mongoQuery')
var result = app => {
	var isEmptyStr = required.isEmptyStr
	var isRange = required.isRange
	var isEmptyObject = required.isEmptyObject
	// addCode
			// Hyp5RsA7H Start
		

// 注册用户
var addUser=(req,res,next)=>{

    var where = {username:req.body.username,password:req.body.password,code:'gui'}
	
  mongodb.save('user', where,function(err, data) {
    if(err) {
      console.log(err)
      send(res, false)
    } else {
      if(data){
        req.userId = data._id
        next()
      }else{
        send(res, false,'注册失败')
      }
    }
  })
}
// 查询用户名是否存在
var isUserName=(req,res,next)=>{

    var where = {username:req.body.username}
	
  mongodb.findOne('user', where,function(err, data) {
    if(err) {
      console.log(err)
      send(res, false)
    } else {
      if(data){
        send(res, false,'该用户已注册')
      }else{
        next()
      }
    }
  })
}
app.post('/register',isUserName,addUser,(req, res) => { // userInfo-静态页面的注册功能
    
  var params = req.body
  var username = params.username // 用户名
    if (isEmptyStr(username)) {
      send(res, false, '用户名不能为空')
      return
    }
  var password = params.password // 密码
    if (isEmptyStr(password)) {
      send(res, false, '密码不能为空')
      return
    }
    
  var where = {}
    
    	if(username !='admin'){
      send(res, false,'用户名不合法')
      return
    }
    where.createTime = new Date()
    where.updateTime = new Date()
	where.userId = req.userId
	where.name = '匿名用户'
	where.introduction = '超级管理员'
	where.avatar = 'https://wpimg.wallstcn.com/f778738c-e4f8-4870-b634-56703b4acafe.gif'
    
    
    
  mongodb.save('userInfo', where,function(err, data) {
    if(err) {
      console.log(err)
      send(res, false)
    } else {
      
      send(res, true, data)
    }
    return
  })
    
    
})
		// Hyp5RsA7H end
			
}
module.exports = result
