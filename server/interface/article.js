var required = require('../plugs/guimain/required')
var authToken = require('../funcs/authTimeOut')
var mongoFunc = require('../funcs/mongoFunc')
var send = require('../funcs/sendData')
var mongodb = require('../plugs/mongose/mongoQuery')
var result = app => {
	var isEmptyStr = required.isEmptyStr
	var isRange = required.isRange
	var isEmptyObject = required.isEmptyObject
	// addCode
			// Bk9VXVvvH Start
			

app.get('/article/data',authToken,(req, res) => { // article-查询
    
  var params = req.query
    
  var where = {}
    
    
    
    
    
  mongodb.findOne('article', where,function(err, data) {
    if(err) {
      console.log(err)
      send(res, false)
    } else {
      
      send(res, true, data)
    }
    return
  })
    
    
})
			// Bk9VXVvvH end
			
			// HJwEQEvwS Start
			

app.post('/article/add',authToken,(req, res) => { // article-新增
    
  var params = req.body
  var title = params.title // 标题
  var info = params.info // 简介
  var content = params.content // 内容
  var auth = params.auth // 作者
  var level = params.level // 推荐等级
    if(!level){
      level = 0
    }
    
  var where = {title,info,content,auth,level}
    
    
    where.createTime = new Date()
    where.updateTime = new Date()
    
        where.userId = req.userId
    
    
  mongodb.save('article', where,function(err, data) {
    if(err) {
      console.log(err)
      send(res, false)
    } else {
      
      send(res, true, data)
    }
    return
  })
    
    
})
			// HJwEQEvwS end
			
			// rJ24QNwwr Start
			

app.post('/article/delete',authToken,(req, res) => { // article-删除
    
  var params = req.body
  var _id = params._id // 数据唯一id
    if (isEmptyStr(_id)) {
      send(res, false, '数据唯一id不能为空')
      return
    }
    
  var where = {_id}
    
    
    
        where.userId = req.userId
    mongoFunc.getId('article', where, res).then(_data=>{
      if(_data){
      
    
      if(isEmptyObject(where)){
        send(res, false);
        return
      } 
  mongodb.remove('article', where,function(err, data) {
    if(err) {
      console.log(err)
      send(res, false)
    } else {
      
      send(res, true, data)
    }
    return
  })
    }else{
        send(res, false,'删除失败，不可重复删除')
      }
    })
    
})
			// rJ24QNwwr end
			
			// S1o4QVvwr Start
			

app.get('/article/list',authToken,(req, res) => { // article-分页查询
    
  var params = req.query
  var start = params.start // 分页起始位置
    if(!start){
      start = 0
    }
  var limit = params.limit // 分页显示条数
    if(!limit){
      limit = 10
    }
    
  var where = {}
    
  var otherWhere = {limit:limit,skip:start,sort:{'createTime':-1}}
    
    otherWhere.limit = parseInt(limit) 
    otherWhere.skip = parseInt(start)
    
    
    
    
  mongodb.where('article', where,otherWhere,function(err, data) {
    if(err) {
      console.log(err)
      send(res, false)
    } else {
      
      mongodb.count('article', where, function (err, data2) {
          if (err) {
            console.log(err)
            send(res, false)
          }
          else {
            var result = {total: data2, list: data}
            send(res, true, result)
          }
        })
        return;
    
      send(res, true, data)
    }
    return
  })
    
    
})
			// S1o4QVvwr end
			
			// ryO47EPDH Start
			

app.post('/article/update',authToken,(req, res) => { // article-修改
    
  var params = req.body
  var _id = params._id // 列表唯一id
    if (isEmptyStr(_id)) {
      send(res, false, '列表唯一id不能为空')
      return
    }
  var title = params.title // 标题
  var info = params.info // 简介
  var content = params.content // 内容
  var auth = params.auth // 作者
  var level = params.level // 推荐等级
    if(!level){
      level = 0
    }
    
  var where = {_id}
    
  var updateParams = {title,info,content,auth,level}
    
    updateParams.updateTime = new Date()
    
        where.userId = req.userId
    mongoFunc.getId('article', where, res).then(_data=>{
      if(_data){
      
    
  mongodb.update('article', where,updateParams,function(err, data) {
    if(err) {
      console.log(err)
      send(res, false)
    } else {
      
      send(res, true, data)
    }
    return
  })
    }else{
        send(res, false,'id没找到')
      }
    })
    
})
			// ryO47EPDH end
			
}
module.exports = result
