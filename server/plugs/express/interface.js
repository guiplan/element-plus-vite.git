var getApp = function() {
  console.log("getApp");
  var express = require('express');
  var app = express();
  var bodyParser = require('body-parser');
  app.use(bodyParser.json({
    limit: '10mb'
  }));
  //这里指定参数使用 json 格式
  app.use(bodyParser.urlencoded({
    extended: true,
    limit: '50mb'
  }));
  //	支持跨域
  app.all('*', function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "X-Requested-With");
    res.header("Access-Control-Allow-Methods", "PUT,POST,GET,DELETE,OPTIONS");
    res.header("X-Powered-By", ' 3.2.1')
    res.header("Content-Type", "application/json;charset=utf-8");
    next();
  });
  this.app = app;
  this.express = express;
}
exports.app = getApp;
