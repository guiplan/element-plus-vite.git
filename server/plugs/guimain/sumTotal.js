var sumTotal = {
	sum(...args) {
		var total = 0
		args.forEach(function(arg) {
			arg = parseInt(arg)
			total += arg
		})
		return total
	},
	sumArr(arr, str) { // 计算数组里的属性值 总和
		var total = 0
		if(!arr||!arr[0]) {
			return total
		}
		arr.forEach(function(arg) {
			arg = parseInt(arg[str])
			total += arg
		})
		return total // 计算出总和
	}
}
module.exports = sumTotal
	//var b = [90.909,11]
	//console.log(a.sum(...b))
	//var arr = [{name:'名称',sum:'145'},{name:'名称2',sum:'155'}]
	//console.log(a.sumArr(arr,'sum'))