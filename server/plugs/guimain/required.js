var isEmptyObject = function (obj) {
  for (var name in obj){
    return false;//返回false，不为空对象
  }
  return true;//返回true，为空对象
}
var isEmptyStr = function (str) { // 判断是否为空
	if(str) {
		return false
	} else {
		return true
	}
}
var isRange = function(str,minlength,maxlength) {
	var len = str.length
	if(maxlength) {
		if (len > minlength && len < maxlength)
		{
			return true
		} else {
			return false
		}
	} else {
		if (len > minlength) {
			return true
		} else {
			return false
		}
	}
}
module.exports = {
	isEmptyStr,
	isRange,
	isEmptyObject
}
