import { createRouter, createWebHistory,createWebHashHistory  } from "vue-router";
import routes from './routers.js'

// 路由信息
// const routes = [
    // {
        // path: "/aaa",
        // name: "aaa",
        // component:  () => import('/@/views/test/test.vue'),
    // }
// ];

// 导出路由
export default createRouter({
  history: createWebHashHistory(), // createWebHashHistory 用#号的形式访问
  routes,
});
