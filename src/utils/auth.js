import Cookies from 'js-cookie'
const TokenKey = 'guiplan'
let gToken = () => {
  const token = Cookies.get(TOKEN_KEY)
  if (token) return token
  else return false
}
let sToken = (token) => {
  Cookies.set(TOKEN_KEY, token, { expires: cookieExpires || 1 })
}

export function getToken() {
  // return Cookies.get(TokenKey) 
  return gToken() // 这里的token统一用libs里的
}

export function setToken(token) {
	return sToken(token)
  // return Cookies.set(TokenKey, token)
}

export function removeToken() {
  return Cookies.remove(TokenKey)
}
