/**
 * Created by zhoushg on 2017/10/16.
 * 封装统一的提示信息
 *
 */
// import {Message} from 'view-design'
import Moment from 'moment'
import { ElMessage as Message } from 'element-plus'
// import Md5 from 'js-md5'

// var Message = {
	// success(a){
		// ElMessage.success({
            // message: '恭喜你，这是一条成功消息',
            // type: 'success'
          // });
	// },
	// warning(){
		
	// },
	// error(){
		
	// }
// }

// 通用的组件 用户可自行修改
function getOptions(n, defaults) { // 对比对象并替换新对象
  var o = {}, p
  for (p in defaults) {
    var z = n[p]
    if (z !== undefined && z !== null) {
      o[p] = n[p]
    } else {
      o[p] = defaults[p]
    }
  }
  return o
}

var guiMain = {}
var toast = { // 信息提示框
  show(a) {
    Message.success(a)
  },
  success(a) {
    Message.success(a)
  },
  warn(a) {
    var defaults = {
      text: '',
      type: 'warn' // text为普通文本 cancel 错误  warn 为警告 success为成功微信插件会出现勾勾
    }
    if (typeof a === 'string') {
      defaults.text = a
      a = defaults
    } else {
      a = getOptions(a, defaults)
    }
    Message.warning(a.text)
  },
  error(a) {
    Message.error(a)
  }
}

guiMain.toast = toast
guiMain.moment = Moment
guiMain.getToken = getToken
guiMain.getOptions = getOptions
guiMain.setStorage = (key, val) => {
  localStorage.setItem(key, val)
}
guiMain.getStorage = (key) => {
  return localStorage.getItem(key)
}
guiMain.removeStorage = (key) => {
  localStorage.removeItem(key)
}
guiMain.replaceUrl = (url, path) => {
  var url = url.replace(/http:\/\/.*?\/(.*)/g, '$1')
  return path + '/' + url
}
guiMain.log = (...data)=>{
  console.log(data) // 测试日志  以后统一用这个开启日志，统一去掉日志 注释掉即可。
}
const gui = {
  install(Vue) {
    // Vue.prototype.$gui = guiMain
	Vue.config.globalProperties.$gui = guiMain // vue3.x 改版了~ 坑
  },
  loading(a) { // 加载中
  },
  alert(a) { // 消息弹窗
  },
  guiMain: guiMain
}

function getToken() {
  // return Md5(Md5(new Date().toString() + Math.random()))
}

export default gui
export {toast, getOptions, getToken}

