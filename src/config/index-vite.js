var config = {
	  title: 'guiplan,element-plus', // 后台标题
	  localUrl:'http://127.0.0.1:8072/',
	  baseUrl: { // api请求基础路径
		dev: '/api', // 开发环境下的接口地址
		pro: '/' // 正式环境接口地址
	  }
}
module.exports = config