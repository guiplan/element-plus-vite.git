/**
 * Created by zhoushg on 2017/8/23.
 */
import axios from '../../libs/axios'
// // import store from '../../store/index'
import {toast} from '../../utils/gui_main'
import config from '/@/config'
// console.log(config)
const baseUrl = process.env.NODE_ENV === 'development' ? config.baseUrl.dev : config.baseUrl.pro // 判断当前环境
console.log(baseUrl)
var axio = new axios(baseUrl)
var getData = (params, callBack, loading)=>{
	axio.request(params).then(function (data) {
		if(callBack){
			callBack(data)
		}
	}).catch(err=>{
		toast.error(err.message)
		if(callBack){
			callBack(false)
		}
	})
}
export default getData
