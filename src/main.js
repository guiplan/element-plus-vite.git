import ElementPlus from 'element-plus'
import 'element-plus/lib/theme-chalk/index.css'
import router from './router/index.js'
import { createApp } from 'vue'
import App from './App.vue'
import './index.css'
import guiMian from '/@/utils/gui_main.js'
var app = createApp(App)
app.use(router)
app.use(guiMian)
app.use(ElementPlus)
app.use(ElementPlus).mount('#app')
