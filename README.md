# element-plus-vite-starter

> A starter kit for Element Plus with Vite

<img width="800" alt="Element Plus" src="https://user-images.githubusercontent.com/10731096/97282764-0726eb80-187a-11eb-9658-6dc98ccb8f8d.png">


## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run dev
```

### Compiles and minifies for production
```
npm run build
```

启动项目之后将3000端口输入到guiplan软件里，软件里即可查看到真实的页面效果，我们可以直接在真实的效果上进行可视化布局，可视化编程。将页面效果、页面布局、交互制作 统统一个页面搞定。拖拽、点击、快捷键即可轻松绘制自己想要的页面，并自动生成代码。
www.guiplan.com 下载
